module adder14(a, b, c, cin, cout);
	parameter LENGTH = 13;
	input [LENGTH:0]a;
	input [LENGTH:0]b;
	input cin;
	output [LENGTH:0]c;
	output cout;
	wire [LENGTH+1:0]carry;

	assign carry[0] = cin;
	assign carry[1] = (a[0]&b[0]) | (b[0]&carry[0]) | (carry[0]&a[0]);
	assign c[0] = (a[0]&~b[0]&~carry[0]) |(~a[0]&b[0]&~carry[0]) | (~a[0]&~b[0]&carry[0]) | (a[0]&b[0]&carry[0]);
	assign carry[2] = (a[1]&b[1]) | (b[1]&carry[1]) | (carry[1]&a[1]);
	assign c[1] = (a[1]&~b[1]&~carry[1]) |(~a[1]&b[1]&~carry[1]) | (~a[1]&~b[1]&carry[1]) | (a[1]&b[1]&carry[1]);
	assign carry[3] = (a[2]&b[2]) | (b[2]&carry[2]) | (carry[2]&a[2]);
	assign c[2] = (a[2]&~b[2]&~carry[2]) |(~a[2]&b[2]&~carry[2]) | (~a[2]&~b[2]&carry[2]) | (a[2]&b[2]&carry[2]);
	assign carry[4] = (a[3]&b[3]) | (b[3]&carry[3]) | (carry[3]&a[3]);
	assign c[3] = (a[3]&~b[3]&~carry[3]) |(~a[3]&b[3]&~carry[3]) | (~a[3]&~b[3]&carry[3]) | (a[3]&b[3]&carry[3]);
	assign carry[5] = (a[4]&b[4]) | (b[4]&carry[4]) | (carry[4]&a[4]);
	assign c[4] = (a[4]&~b[4]&~carry[4]) |(~a[4]&b[4]&~carry[4]) | (~a[4]&~b[4]&carry[4]) | (a[4]&b[4]&carry[4]);
	assign carry[6] = (a[5]&b[5]) | (b[5]&carry[5]) | (carry[5]&a[5]);
	assign c[5] = (a[5]&~b[5]&~carry[5]) |(~a[5]&b[5]&~carry[5]) | (~a[5]&~b[5]&carry[5]) | (a[5]&b[5]&carry[5]);
	assign carry[7] = (a[6]&b[6]) | (b[6]&carry[6]) | (carry[6]&a[6]);
	assign c[6] = (a[6]&~b[6]&~carry[6]) |(~a[6]&b[6]&~carry[6]) | (~a[6]&~b[6]&carry[6]) | (a[6]&b[6]&carry[6]);
	assign carry[8] = (a[7]&b[7]) | (b[7]&carry[7]) | (carry[7]&a[7]);
	assign c[7] = (a[7]&~b[7]&~carry[7]) |(~a[7]&b[7]&~carry[7]) | (~a[7]&~b[7]&carry[7]) | (a[7]&b[7]&carry[7]);
	assign carry[9] = (a[8]&b[8]) | (b[8]&carry[8]) | (carry[8]&a[8]);
	assign c[8] = (a[8]&~b[8]&~carry[8]) |(~a[8]&b[8]&~carry[8]) | (~a[8]&~b[8]&carry[8]) | (a[8]&b[8]&carry[8]);
	assign carry[10] = (a[9]&b[9]) | (b[9]&carry[9]) | (carry[9]&a[9]);
	assign c[9] = (a[9]&~b[9]&~carry[9]) |(~a[9]&b[9]&~carry[9]) | (~a[9]&~b[9]&carry[9]) | (a[9]&b[9]&carry[9]);
	assign carry[11] = (a[10]&b[10]) | (b[10]&carry[10]) | (carry[10]&a[10]);
	assign c[10] = (a[10]&~b[10]&~carry[10]) |(~a[10]&b[10]&~carry[10]) | (~a[10]&~b[10]&carry[10]) | (a[10]&b[10]&carry[10]);
	assign carry[12] = (a[11]&b[11]) | (b[11]&carry[11]) | (carry[11]&a[11]);
	assign c[11] = (a[11]&~b[11]&~carry[11]) |(~a[11]&b[11]&~carry[11]) | (~a[11]&~b[11]&carry[11]) | (a[11]&b[11]&carry[11]);
	assign carry[13] = (a[12]&b[12]) | (b[12]&carry[12]) | (carry[12]&a[12]);
	assign c[12] = (a[12]&~b[12]&~carry[12]) |(~a[12]&b[12]&~carry[12]) | (~a[12]&~b[12]&carry[12]) | (a[12]&b[12]&carry[12]);
	assign carry[14] = (a[13]&b[13]) | (b[13]&carry[13]) | (carry[13]&a[13]);
	assign c[13] = (a[13]&~b[13]&~carry[13]) |(~a[13]&b[13]&~carry[13]) | (~a[13]&~b[13]&carry[13]) | (a[13]&b[13]&carry[13]);
	assign cout = carry[14];

	always @(a or b)begin
		#1
		;
		//$display("a=%b, b = %b, a+b = %b", a, b, c);
	end

endmodule
