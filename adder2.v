module adder(a, b, c);
	parameter LENGTH = 8;
	input [LENGTH-1:0]a;
	input [LENGTH-1:0]b;
	output reg [LENGTH-1:0]c;
	
	integer i;
	reg in, out;
	
	always@(a or b) begin
		in = 0;	
		//$display("a=%b, b = %b", a, b);
		for(i = 0; i < 8; i = i + 1)begin
			add(a[i], b[i], in, c[i], out);
			in = out;
		end
		$display("a=%b, b = %b, a+b = %b", a, b, c);
	end

	
task add(input a, input b, input cin, output reg sum, output reg cout);
	begin
		sum = (a&~b&~cin) | (~a&b&~cin) | (~a&~b&cin) | (a&b&cin);
		cout = (a&b) | (b&cin) | (cin&a);
	end
endtask

endmodule