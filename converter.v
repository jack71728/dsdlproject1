module converter(in14, control, out);
	input [13:0] in14;
	input [1:0] control;
	output reg [34:0] out;
	
	reg [13:0] in_to_14;
	
	wire carry_out;
	wire [34:0] out14;
	wire [13:0] d14_0, d14_1, d14_2, d14_3, d14_4;
	wire [6:0] origin, minus_num;
	
	assign minus_num = in14[6:0];
	
	converter14 c14_0(.in14(in_to_14), .out(out14[6:0]), .div14(d14_0));
	converter14 c14_1(.in14(d14_0), .out(out14[13:7]), .div14(d14_1));
	converter14 c14_2(.in14(d14_1), .out(out14[20:14]), .div14(d14_2));
	converter14 c14_3(.in14(d14_2), .out(out14[27:21]), .div14(d14_3));
	converter14 c14_4(.in14(d14_3), .out(out14[34:28]), .div14(d14_4));
	
	subber sub(.a(minus_num), .notb(7'b1), .c(origin), .cout(carry_out));
	
	always @(in14)begin
		if(~control[1])begin
			if(~control[0])begin//+
				in_to_14 = in14;
				#1000
				out = out14;
			end
			else begin//-
				if(~in14[6])begin//positive num
					in_to_14 = in14;
					out = out14;
				end
				else begin
					#1000
					in_to_14 = {{7{1'b0}}, ~origin};
					#2000
					out = out14;
				end
			end
		end
		else begin
			if(~control[0])begin//*
				in_to_14 = in14;
				#1000
				out = out14;
			end
			else begin//%
				in_to_14 = in14;
				#1000
				out = out14;
			end
		end
		#100
		;
	end

endmodule
/*

00: + 01: - 10: x ??11: %

*/