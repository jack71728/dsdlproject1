module converter14(in14, out, div14);
	input [13:0] in14;
	output [6:0] out;
	output [13:0] div14;
	
	wire [6:0] temp_3, temp_2, temp_1, temp_0;
	wire [6:0] in13_7, in10_4, in7_1, in6_0;
	wire [6:0] temp_div3, temp_div2, temp_div1, temp_div0;
	wire [13:0] temp_div3_14, temp_div2_14, temp_div1_14, temp_div0_14;
	wire [13:0] temp_add1, temp_add0;
	wire carry_out;
	
	assign in13_7 = in14[13:7];
	assign in10_4 = {temp_3[3:0], in14[6:4]};
	assign in7_1 = {temp_2[3:0], in14[3:1]};
	assign in6_0 = {1'b0, 1'b0, temp_1[3:0], in14[0]};
	
	assign temp_div3_14 = { temp_div3, {7{1'b0}} };
	assign temp_div2_14 = { {3{1'b0}}, temp_div2, {4{1'b0}}};
	assign temp_div1_14 = { {6{1'b0}}, temp_div1, {1{1'b0}}};
	assign temp_div0_14 = { {7{1'b0}}, temp_div0 };
	
	modder mod3(.a(in13_7), .b(7'b0001010), .c(temp_3), .div(temp_div3));
	modder mod2(.a(in10_4), .b(7'b0001010), .c(temp_2), .div(temp_div2));
	modder mod1(.a(in7_1), .b(7'b0001010), .c(temp_1), .div(temp_div1));
	modder mod0(.a(in6_0), .b(7'b0001010), .c(temp_0), .div(temp_div0));
	
	adder14 add2(.a(temp_div3_14), .b(temp_div2_14), .c(temp_add1), .cin(1'b0), .cout(carry_out));
	adder14 add1(.a(temp_div1_14), .b(temp_div0_14), .c(temp_add0), .cin(1'b0), .cout(carry_out));
	adder14 add0(.a(temp_add1), .b(temp_add0), .c(div14), .cin(1'b0), .cout(carry_out));
	
	seven_seg seven(.in(temp_0[3:0]), .out(out));
	
	always @(in14)begin
		#80
		;
	end

endmodule
/*

00: + 01: - 10: x ??11: %

*/