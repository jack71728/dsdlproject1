module converter8(in8, out, div8);
	input [7:0] in8;
	output [6:0] out;
	output [7:0] div8;
	
	wire [6:0] in7_1, in6_0;
	wire [6:0] temp_1, temp_0, temp_div1, temp_div0;
	
	assign in7_1 = {in8[7:1]};
	assign in6_0 = {{2{1'b0}}, temp_1[3:0], in8[0]};
	
	modder mod1(.a(in7_1), .b(7'b0001010), .c(temp_1), .div(temp_div1));
	modder mod0(.a(in6_0), .b(7'b0001010), .c(temp_0), .div(temp_div0));
	
	adder add(.a(temp_div1), .b(temp_div0), .c(div8[6:0]), .cin(1'b0), .cout(div8[7]));
	
	seven_seg seven(.in(temp_0[3:0]), .out(out));
	
	always @(in8)begin
		#80
		;
	end

endmodule
/*

00: + 01: - 10: x ??11: %

*/