module main(input1, input2, control, output14, minus, LED, LEDP);
	input [6:0] input1;
	input [6:0] input2;
	input [1:0] control;
	output reg [13:0] output14;
   output [55:0] LED; //7-seg.
	output [7:0] LEDP; //point
	
	output reg minus;
	reg neg;
	wire [6:0] temp_sum, temp_diff, temp_mod, temp_div;
	wire [13:0] temp_pro;
	wire carry_out, minus_signal;
	wire [34:0] LEDOUT;
	
	assign LEDP = {8{1'b1}};
	assign LED = {{17{1'b1}}, neg, {3{1'b1}}, ~LEDOUT};
	
	adder add(.a(input1), .b(input2), .c(temp_sum), .cin(0), .cout(carry_out));
	subber sub(.a(input1), .notb(input2), .c(temp_diff), .cout(minus_signal));
	producter pro(.a(input1), .b(input2), .c(temp_pro));
	modder mod(.a(input1), .b(input2), .c(temp_mod), .div(temp_div));
	converter c(.in14(output14), .control(control), .out(LEDOUT));
	
	always @(input1 or input2 or control)begin
		if(~control[1])begin
			if(~control[0])begin //00 + 
				#25
				output14 = { {6{1'b0}}, carry_out, temp_sum };
				neg = 1'b1;
				minus = 1'b0;
				//#1
				//$display("main x=%d, y = %d, x+y = %d", input1, input2, output14);
			end
			else begin //01 - 
				#25
				output14 = {{7{1'b0}}, temp_diff};
				if(output14[6])begin
					neg = 0;
				end
				else begin
					neg = 1;
				end
				minus = minus_signal;
				//#1
				//if(minus_signal)
				//	$display("overflow!!!!");
				//$display("main x=%d, y = %d, x-y = %d", input1, input2, output14);
			end
		end
		else begin
			if(~control[0])begin //10 x
				#300
				output14 = temp_pro;
				neg = 1'b1;
				minus = 1'b0;
			end
			else begin //11 %
				#5000
				output14 = {{7{1'b0}}, temp_mod};
				neg = 1'b1;
				minus = 1'b0;
			//	#1
			//	;
				//$display("main x=%d, y = %d, x mod y = %d, div = %d", input1, input2, output14, temp_div);
			end
		end
	end

endmodule


/*

00: + 01: - 10: x ??11: %

*/