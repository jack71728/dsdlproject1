module modder(a, b, c, div);
	parameter LENGTH = 7;
	input [LENGTH-1:0] a;
	input [LENGTH-1:0] b;
	output reg [LENGTH-1:0] c;
	output reg [LENGTH-1:0] div;
	reg signed [13:0] input0, input1, input2, input3, input4, input5, input6;
	reg signed [13:0] input00, input11, input22, input33, input44, input55, input66;
	reg signed [13:0] temp14;
	wire [13:0] temp_dif0, temp_dif1, temp_dif2, temp_dif3, temp_dif4, temp_dif5, temp_dif6, a14, b14;
	wire carry_out;

	assign a14 = {{7{1'b0}}, a};
	assign b14 = {{7{1'b0}}, b};
	adder14 sub6(.a(input6) , .b(~input66), .c(temp_dif6), .cin(1'b1), .cout(carry_out));
	adder14 sub5(.a(input5) , .b(~input55), .c(temp_dif5), .cin(1'b1), .cout(carry_out));
	adder14 sub4(.a(input4) , .b(~input44), .c(temp_dif4), .cin(1'b1), .cout(carry_out));
	adder14 sub3(.a(input3) , .b(~input33), .c(temp_dif3), .cin(1'b1), .cout(carry_out));
	adder14 sub2(.a(input2) , .b(~input22), .c(temp_dif2), .cin(1'b1), .cout(carry_out));
	adder14 sub1(.a(input1) , .b(~input11), .c(temp_dif1), .cin(1'b1), .cout(carry_out));
	adder14 sub0(.a(input0) , .b(~input00), .c(temp_dif0), .cin(1'b1), .cout(carry_out));
	
	always @(a or b)begin
		//#3
		div = {(LENGTH){1'b0}};
		input6 = a14;
		
		input66 = b14 << 6;
		#250
		if(~temp_dif6[13])begin
			input5 = temp_dif6;
			//$display("6 new input1 = %d", input1);
			div[6] = 1'b1;
		end
		else begin
			input5 = input6;
		end
		
		input55 = b14 << 5;//5
		#250
		if(~temp_dif5[13])begin
			input4 = temp_dif5;
			//$display("5 new input1 = %d", input1);
			div[5] = 1'b1;
		end
		else begin
			input4 = input5;
		end
		
		input44 = b14 << 4;//4
		#250
		if(~temp_dif4[13])begin
			input3 = temp_dif4;
			//$display("4 new input1 = %d", input1);
			div[4] = 1'b1;
		end
		else begin
			input3 = input4;
		end
		
		input33 = b14 << 3;//3
		#250
		if(~temp_dif3[13])begin
			input2 = temp_dif3;
			//$display("3 new input1 = %d", input1);
			div[3] = 1'b1;
		end
		else begin
			input2 = input3;
		end
		
		input22 = b14 << 2;//2
		#250
		if(~temp_dif2[13])begin
			input1 = temp_dif2;
			//$display("2 new input1 = %d", input1);
			div[2] = 1'b1;
		end
		else begin
			input1 = input2;
		end
		
		input11 = b14 << 1;//1
		#250
		if(~temp_dif1[13])begin
			input0 = temp_dif1;
			//$display("1 new input1 = %d", input1);
			div[1] = 1'b1;
		end
		else begin
			input0 = input1;
		end
		
		input00 = b14 << 0;//0
		#250
		if(~temp_dif0[13])begin
			temp14 = temp_dif0;
			//$display("0 new input1 = %d", input1);
			div[0] = 1'b1;
		end
		else begin
			temp14 = input0;
		end
		
		#250
		//$display("new input1 = %d", input1);
		c = temp14[6:0];
	end

endmodule



