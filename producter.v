

module producter(a, b, c);
	input [6:0]a;
	input [6:0]b;
	output [13:0]c;
	wire [13:0] a14;

	wire carry_out;

	assign a14 = {{7{1'b0}}, a};
	
	reg [13:0] tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp01, tmp23, tmp45, tmp601, tmp2345;
	wire [13:0] w_tmp01, w_tmp23, w_tmp45, w_tmp601, w_tmp2345;
	
	adder14 add01(.a(tmp0), .b(tmp1), .c(w_tmp01), .cin(0), .cout(carry_out));
	adder14 add23(.a(tmp2), .b(tmp3), .c(w_tmp23), .cin(0), .cout(carry_out));
	adder14 add45(.a(tmp4), .b(tmp5), .c(w_tmp45), .cin(0), .cout(carry_out));
	adder14 add601(.a(tmp6), .b(tmp01), .c(w_tmp601), .cin(0), .cout(carry_out));
	adder14 add2345(.a(tmp23), .b(tmp45), .c(w_tmp2345), .cin(0), .cout(carry_out));
	adder14 add(.a(tmp601), .b(tmp2345), .c(c), .cin(0), .cout(carry_out));

	always @(a or b)begin
		if (b[0]) begin
			tmp0 = a14;
	  	end
	  	else begin
	  		tmp0 = { {14{1'b0}} };
	  	end
	  	
		if (b[1]) begin
			tmp1 = (a14 << 1);
		end
		else begin
	  		tmp1 = { {14{1'b0}} };
	  	end
	  	
		if (b[2]) begin
			tmp2 = (a14 << 2);
	  	end
	  	else begin
	  		tmp2 = { {14{1'b0}} };
	  	end
	  	
		if (b[3]) begin
			tmp3 = (a14 << 3);
	  	end
	  	else begin
	  		tmp3 = { {14{1'b0}} };
	  	end
	  	
		if (b[4]) begin
			tmp4 = (a14 << 4);
	  	end
	  	else begin
	  		tmp4 = { {14{1'b0}} };
	  	end
	  	
		if (b[5]) begin
			tmp5 = (a14 << 5);
	  	end
	  	else begin
	  		tmp5 = { {14{1'b0}} };
	  	end
	  	
		if (b[6]) begin
			tmp6 = (a14 << 6);
	  	end
	  	else begin
	  		tmp6 = { {14{1'b0}} };
	  	end
	  	
	  	#100
	  	
	  	tmp01 = w_tmp01;
	  	tmp23 = w_tmp23;
	  	tmp45 = w_tmp45;
	  	
	  	#80
	  	tmp601 = w_tmp601;
	  	tmp2345 = w_tmp2345;
	  	
	  	#50
		;
		//i = a*b;
		//$display("a = %d, b = %d, a*b = %d, ans = %d", a, b, c, i);
	end

endmodule






