module subber(a, notb, c, cout);
	parameter LENGTH = 7;
	input [LENGTH-1:0]a;
	input [LENGTH-1:0]notb;
	output [LENGTH-1:0]c;
	output reg cout;
	wire [LENGTH:0]carry;
	wire [LENGTH-1:0]b;

	assign b = ~notb;
	
	assign carry[0] = 1;
	assign c[0] = (a[0]&~b[0]&~carry[0]) |(~a[0]&b[0]&~carry[0]) | (~a[0]&~b[0]&carry[0]) | (a[0]&b[0]&carry[0]);
	assign carry[1] = (a[0]&b[0]) | (b[0]&carry[0]) | (carry[0]&a[0]);
	assign c[1] = (a[1]&~b[1]&~carry[1]) |(~a[1]&b[1]&~carry[1]) | (~a[1]&~b[1]&carry[1]) | (a[1]&b[1]&carry[1]);
	assign carry[2] = (a[1]&b[1]) | (b[1]&carry[1]) | (carry[1]&a[1]);
	assign c[2] = (a[2]&~b[2]&~carry[2]) |(~a[2]&b[2]&~carry[2]) | (~a[2]&~b[2]&carry[2]) | (a[2]&b[2]&carry[2]);
	assign carry[3] = (a[2]&b[2]) | (b[2]&carry[2]) | (carry[2]&a[2]);
	assign c[3] = (a[3]&~b[3]&~carry[3]) |(~a[3]&b[3]&~carry[3]) | (~a[3]&~b[3]&carry[3]) | (a[3]&b[3]&carry[3]);
	assign carry[4] = (a[3]&b[3]) | (b[3]&carry[3]) | (carry[3]&a[3]);
	assign c[4] = (a[4]&~b[4]&~carry[4]) |(~a[4]&b[4]&~carry[4]) | (~a[4]&~b[4]&carry[4]) | (a[4]&b[4]&carry[4]);
	assign carry[5] = (a[4]&b[4]) | (b[4]&carry[4]) | (carry[4]&a[4]);
	assign c[5] = (a[5]&~b[5]&~carry[5]) |(~a[5]&b[5]&~carry[5]) | (~a[5]&~b[5]&carry[5]) | (a[5]&b[5]&carry[5]);
	assign carry[6] = (a[5]&b[5]) | (b[5]&carry[5]) | (carry[5]&a[5]);
	assign c[6] = (a[6]&~b[6]&~carry[6]) |(~a[6]&b[6]&~carry[6]) | (~a[6]&~b[6]&carry[6]) | (a[6]&b[6]&carry[6]);
	assign carry[7] = (a[6]&b[6]) | (b[6]&carry[6]) | (carry[6]&a[6]);
	
	always @(a or b)begin
		#10
		if((a[6]&~notb[6]&~c[6])|(~a[6]&notb[6]&c[6]))begin
			cout = 1;	
		end
		else begin
			cout = 0;
		end
		
		//$display("a=%b, b = %b, a-b = %b", a, b, c);
	end

endmodule