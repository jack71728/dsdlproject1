module subber1(a, b, c);
	parameter LENGTH = 7;
	input [LENGTH-1:0]a;
	input [LENGTH-1:0]b;
	output [LENGTH-1:0]c;
	wire carry_out;
	adder1 fake_add(a, ~b, c, 1, carry_out);
	
	always @(a or b)begin
		#1
		;
		//$display("sub a=%b, b = %b, a-b = %b", a, b, c);
	end

endmodule