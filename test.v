module test;
	parameter WIDTH=8, NUM=32;
	reg [WIDTH-1:0] data[NUM-1:0];
	reg signed [WIDTH-1:0] x, y, init_x;
	reg signed [6:0] diff, signed_y, signed_x;
	wire [13:0] out;
	reg [1:0] control;
	integer i;
	
	main main1(x[6:0], y[6:0], control, out);
	
	initial begin
		$readmemh("input", data, 0, NUM-1);
		control = 2'b00;
		for(i = 0; i <= NUM-1; i = i + 2)begin
			
			#2000000
			
			#1
			x = data[i];
			init_x = x;
			y = data[i+1];
			control = 2'b00;
			#50
			$display("x=%d, y = %d, x+y = %d", x[6:0], y[6:0], out[6:0]);
			
			control = 2'b01;
			#50
			diff = out[6:0];
			signed_x = x[6:0];
			signed_y = y[6:0];
			$display("x=%d, y = %d, x-y = %d", signed_x, signed_y, diff);
			
			control = 2'b10;
			#50
			$display("x=%d, y = %d, x*y = %d", x[6:0], y[6:0], out);
			
			control = 2'b11;
			#3000//XDD
			$display("x=%d, y = %d, x mod y = %d", init_x[6:0], y[6:0], out[6:0]);
		end
	$finish;
	end
endmodule
