module test1;
	parameter WIDTH=14, NUM=32;
	reg [WIDTH-1:0] data[NUM-1:0];
	reg [WIDTH-1:0] x, y;
	wire [WIDTH-1:0] out;
	integer i;
	wire carry;
	
	adder14 add(x, y, out, 1, carry);
	
	initial begin
		$readmemh("input", data, 0, NUM-1);
		for(i = 0; i <= NUM-1; i = i + 2)begin
			x = data[i];
			y = data[i+1];			
			#50
			$display("x=%d, y = %d, x+y = %d, ans = %d", x, y, out, x+y);
		end
	$finish;
	end
endmodule
